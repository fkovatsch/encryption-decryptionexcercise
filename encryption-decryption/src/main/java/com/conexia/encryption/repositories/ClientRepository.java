package com.conexia.encryption.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.conexia.encryption.entities.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

}
