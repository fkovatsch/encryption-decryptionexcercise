package com.conexia.encryption.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Client {
	
	private Long id;
	private String publicKey;
	private String action;

}
